const https = require("https");

module.exports = {
  payloadList: [],

  getPayload(instanceId) {
    return this.payloadList.find((obj) => {
      return obj.instanceId === instanceId;
    });
  },

  addPayload(instanceId, payload) {
    let obj = this.getPayload(instanceId);
    if (!obj) {
      this.payloadList.push({ instanceId, payload });
    }
    obj = this.getPayload(instanceId);
    if (obj) {
      obj.payload = payload;
      if (obj.interval) {
        clearInterval(obj.interval);
      }
      obj.interval = setInterval(() => {
        this.getData(payload);
      }, payload.updateInterval);
    }
  },

  async fetchUrl(url, options, type) {
    let text = "";
    await new Promise((resolve, reject) => {
      const req = https.get(url, options, (res) => {
        let data = [];
        res.on("data", (chunk) => {
          data.push(chunk);
        });

        res.on("end", () => {
          text = Buffer.concat(data).toString();
          if (res.statusCode < 200 || res.statusCode > 299) {
            console.error(
              `${this.name}: Requesting ${url} failed with status code ${res.statusCode} ${text}`
            );
          } else {
            if (type === "json") {
              if (text !== "") text = JSON.parse(text);
            }
            resolve(text);
          }
        });
      });
      req.on("error", (err) => {
        console.log(`${this.name}: Request failed ${err.message}`);
        reject(err);
      });
    });
    return text;
  },

  async fetchJson(url, options) {
    return await this.fetchUrl(url, options, "json");
  },

  async fetchText(url, options) {
    return await this.fetchUrl(url, options, "text");
  },
};
