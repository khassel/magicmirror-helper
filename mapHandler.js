const getModuleHeader = (data, config) => {
  if (
    (data.header && data.header !== undefined) ||
    config.title ||
    config.logoTitle
  ) {
    let title = `${data.header}`.replace("undefined", "");
    if (title === "") title =`${config.title}`;
    return `<i class="${config.logoTitle} logo"></i><span>${title}</span>`;
  } else {
    return "";
  }
};

class mapHandler {
  constructor(config, name, htmlElement, styleFunction) {
    let countries = null;
    let cities = null;

    const checkLayer = (layer) => {
      if (layer && layer.font && layer.color) {
        return true;
      } else {
        return false;
      }
    };

    const raster = new ol.layer.Tile({
      source: new ol.source.XYZ({
        declutter: true,
        url: config.mapUrl,
        attributions: config.mapAttributions,
      }),
    });

    raster.on("error", (err) => {
      console.error(`${name} raster error: ${err}`);
    });

    if (checkLayer(config.layerCountries)) {
      const countryStyle = new ol.style.Style({
        text: new ol.style.Text({
          font: config.layerCountries.font,
          fill: new ol.style.Fill({
            color: config.layerCountries.color,
          }),
        }),
        stroke: new ol.style.Stroke({
          color: "rgba(0, 0, 0, 0.7)",
          width: 0.5,
        }),
      });

      countries = new ol.layer.Vector({
        source: new ol.source.Vector({
          url: "https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_110m_admin_0_countries.geojson",
          format: new ol.format.GeoJSON(),
        }),
        style: (feature) => {
          countryStyle.getText().setText(feature.get("name"));
          return countryStyle;
        },
        declutter: true,
      });
    }

    if (checkLayer(config.layerCities)) {
      const cityStyle = new ol.style.Style({
        text: new ol.style.Text({
          textAlign: "left",
          textBaseline: "bottom",
          font: config.layerCities.font,
          fill: new ol.style.Fill({
            color: config.layerCities.color,
          }),
        }),
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: config.layerCities.color,
          }),
          radius: 1.5,
        }),
      });

      cities = new ol.layer.Vector({
        source: new ol.source.Vector({
          url: "https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_110m_populated_places_simple.geojson",
          format: new ol.format.GeoJSON(),
        }),
        style: (feature) => {
          const txtStyle = cityStyle.getText();
          const txt = feature.get("name");
          txtStyle.setText(txt);
          txtStyle.offsetX_ = txt.length;
          return cityStyle;
        },
        declutter: true,
      });
    }

    this.sourcePoints = new ol.source.Vector();

    const layerPoints = new ol.layer.Vector({
      source: this.sourcePoints,
      style: styleFunction,
    });

    this.map = new ol.Map({
      controls: ol.control.defaults
        .defaults()
        .extend([new ol.control.ScaleLine()]),
      layers: [raster, layerPoints],
      target: htmlElement,
      view: new ol.View({
        zoom: config.mapZoom,
      }),
    });
    if (countries) this.map.addLayer(countries);
    if (cities) this.map.addLayer(cities);

    this.map.on("change:size", () => {
      setTimeout(() => {
        const zoom = this.map.getView().getZoom();
        this.map.getView().setZoom(zoom + 1);
        this.map.getView().setZoom(zoom);
      }, 500);
    });

    this.map.on("error", (err) => {
      console.error(`${name} map error: ${err}`);
    });
  }

  addMapPoint = (lon, lat, txt, bearing) => {
    const pointFeature = new ol.Feature({
      geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])),
      text: txt,
      deg: bearing,
    });
    this.sourcePoints.addFeature(pointFeature);
  };

  clearPoints = () => {
    this.sourcePoints.clear(true);
  };
}
